package com.example.agenda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton

class TambahAgenda : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_tambah_agenda, container, false)
        val btnOk : ImageButton = v.findViewById(R.id.imgBtnOK) as ImageButton
        val btnClose : ImageButton = v.findViewById(R.id.imageButton2) as ImageButton
        val editText : EditText = v.findViewById(R.id.edittxtTambah) as EditText

        btnOk.setOnClickListener {

            // TODO 1: tulis untuk tambah ke sql dari dari edit text

            val fragNext = LayarUtama()
            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayoutUtama,fragNext)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        }

        btnClose.setOnClickListener {

            editText.text.clear()

            val fragNext = LayarUtama()
            val fragmentManager = activity!!.supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayoutUtama,fragNext)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.title = "Tambah Agenda"
    }

}