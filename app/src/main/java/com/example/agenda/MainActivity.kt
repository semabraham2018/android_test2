package com.example.agenda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmenPertama = LayarUtama()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frameLayoutUtama,fragmenPertama).commit()
        }


    }
}